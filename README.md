This extension adds two types of fusion reactors as station modules, so that stations are no longer reliant on sunshine
to produce energy cells.

### Deuterium-Deuterium Fusion Reactor

This type of reactor uses hydrogen alone to produce energy cells. As it can only fuse hydrogen it produces helium as a
byproduct.

### Deuterium-Helium Fusion Reactor

This type of reactor uses hydrogen and helium to produce more energy cells as the D-D reactor. There is no byproduct,
but it requires both elements.

# Blueprints

Both reactors are close-kept secrets of the below factions, so high reputation is required to buy them.
Their insane shielding also requires a high amount of resources to build.

You can buy blueprints for these reactors at the following faction representatives:

- Alliance of the Word
- Antigone Republic
- Argon Federation
- Holy Order of the Pontifex
- Ministry of Finance
- Godrealm of the Paranid
- Teladi Company
- Realm of the Trinity
- Hatikvah Free League
- Court/Cabal of Curbs (Split Vendetta DLC)
- Free Families (Split Vendetta DLC)
- Zyarth/Rhak Patriarchy (Split Vendetta DLC)
- Vigor Syndicate (Tides of Avarice DLC)
- Riptide Rakers (Tides of Avarice DLC)

I might release a second mod for Cradle of Humanity with specific Terran-economy balanced values.

# Other Factions

The modules are not integrated in any of the faction factories as it is unclear what major impact they would have on the
economy as a whole.

# Thanks

ShadowDragon8685 came up with the idea on the Egosoft Discord and helped calculate production values.

ColdRogue26 helped me find a useful model and tested the reactors.

Thank you both so much!

# Contributions

If you'd like to contribute and for example help balance this mod, or maybe help figure out how to have factions build
them on their own, head over to https://gitlab.com/bit-refined/x4-extensions/fusion-reactor to get started. You can also
contact me on the Egosoft Discord server :) 
